import java.util.Scanner;

public class YourGrade {
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        System.out.print("Please input your score: ");
        int grade = kb.nextInt();
        if ( grade >= 80) {
            System.out.println("Grade: A");
        }else if ( grade >= 75) {
            System.out.println("Grade: B+");
        }else if ( grade >= 70) {
            System.out.println("Grade: B");
        }else if ( grade >= 65) {
            System.out.println("Grade: C+");
        }else if ( grade >= 60) {
            System.out.println("Grade: C");
        }else if ( grade >= 55) {
            System.out.println("Grade: D+");
        }else if ( grade >= 50) {
            System.out.println("Grade: D");
        }else {
            System.out.println("Grade: F");
        }
    }
}
